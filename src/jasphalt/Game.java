package jasphalt;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

import java.util.*;

import static jasphalt.Utils.setImage;

public class Game {


    Random random = new Random();

    private Road road;

    private Player player;

    private final Object finalizerMonitor = new Object();
    private final Object updaterMonitor = new Object();


    double vRoad;
    double vOncoming;

    private int frameCount;
    private int fpsCurrent;
    private long previousTime = -1;

    private long score;

    private HashMap<Integer, Image> cloudImage = new HashMap<>();
    private HashMap<Integer, Image> oncomingCarImage = new HashMap<>();
    private HashMap<Integer, Image> passingCarImage = new HashMap<>();


    private List<AbstractSprite> cloudSpriteList = new ArrayList<>();
    private List<AbstractSprite> carSpriteList = new ArrayList<>();
    private List<AbstractSprite> playerList = new ArrayList<>();

    private LinkedList<GameState> gameStates = new LinkedList<>();

    private boolean botMode;

    private boolean replayMode;

    private boolean gameOver;

    private int difficultyLevel;

    private int spawnedCar;

    private Scene scene;

    Pane backgroudLayer;
    Pane playfieldLayer;
    Pane upperCloudLayer;
    Pane lowerCloudLayer;
    Pane debugLayer;

    Label informationLabel;

    public Game(int difficultyLevel) {

        Group root = new Group();

        this.difficultyLevel = difficultyLevel;

        backgroudLayer = new Pane();
        playfieldLayer = new Pane();
        debugLayer = new Pane();
        lowerCloudLayer = new Pane();
        upperCloudLayer = new Pane();

        createDebugOverlay();

        root.getChildren().addAll(backgroudLayer, playfieldLayer, debugLayer,
                lowerCloudLayer, upperCloudLayer);

        scene = new Scene(root, Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT);


        initializeInput();

        loadGame();


    }


    public void startGame() {

        createPlayer();


        Timeline gameLoop = new Timeline();
        gameLoop.setCycleCount(Timeline.INDEFINITE);

        Updater updater = new Updater(informationLabel, updaterMonitor,
                difficultyLevel, score, road);

        updater.start();


        informationLabel.setText("Score: 10");


        KeyFrame keyFrame = new KeyFrame(Duration.seconds(0.016), actionEvent -> {

//            road.updateRoad();

            player.move();

            for (AbstractSprite sprite : carSpriteList) {


                if (player.checkCollisions(sprite)) {

                    synchronized (finalizerMonitor) {

                        gameOver = true;
                        updater.kill();
                        finalizerMonitor.notifyAll();
                        gameLoop.stop();
                    }

                }

            }

            for (AbstractSprite sprite : carSpriteList) {

                player.checkBotCollisions(sprite);

            }


            player.checkBounds();

            player.update();


            spawnClouds();

            checkRemovability(cloudSpriteList);

            moveSprites(cloudSpriteList);

            removeSprites(cloudSpriteList);

            renderSprites(cloudSpriteList);


            spawnCar();


            checkRemovability(carSpriteList);

            moveSprites(carSpriteList);

            removeSprites(carSpriteList);

            renderSprites(carSpriteList);


            updateFps();

            updateScore();


            if (!carSpriteList.isEmpty()) {
                gameStates.add(new GameState(player, (CarSprite) carSpriteList.get(0),
                        Long.parseLong(informationLabel.getText().replaceAll("[^0-9]", "")), difficultyLevel));
            }

        });


        gameLoop.getKeyFrames().add(keyFrame);
        gameLoop.play();

        Finalizer finalizer = new Finalizer(replayMode, finalizerMonitor);

        finalizer.start();

        System.out.println("outOfTimer");

    }

    public void replay(String name) {

        createPlayer();

        Reader reader = new Reader(name);

        reader.start();

        try {
            reader.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        gameStates = reader.getStates();

        difficultyLevel = gameStates.get(0).getDifficulty();

        Iterator<GameState> iterator = gameStates.iterator();

        Updater updater = new Updater(informationLabel, updaterMonitor,
                gameStates.get(0).getDifficulty(), score, road);

        updater.start();

        int scoreDelta;

        if (difficultyLevel == Constants.HARD_LEVEL) {

            scoreDelta = 10;

        } else {

            scoreDelta = 0;

        }

        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);

        KeyFrame keyFrame = new KeyFrame(Duration.seconds(0.016), actionEvent -> {

            if (!iterator.hasNext()) {

                synchronized (finalizerMonitor) {

                    finalizerMonitor.notifyAll();

                }

                updater.kill();
                timeline.stop();

            }

            GameState state = iterator.next();

//            road.updateRoad();

            player.setX(state.getPlayerX());
            player.setY(state.getPlayerY());

            player.update();

            spawnClouds();

            checkRemovability(cloudSpriteList);

            moveSprites(cloudSpriteList);

            removeSprites(cloudSpriteList);

            renderSprites(cloudSpriteList);


            respawnCar(state);

            checkRemovability(carSpriteList);

            moveSprites(carSpriteList);

            removeSprites(carSpriteList);

            renderSprites(carSpriteList);

            updateFps();
            updateScore();

            informationLabel.setText("Score: " + String.valueOf(state.getScore() + scoreDelta));


        });

        timeline.getKeyFrames().add(keyFrame);
        timeline.play();


        Finalizer finalizer = new Finalizer(replayMode, finalizerMonitor);

        finalizer.start();


    }


    private void respawnCar(GameState state) {

        if (!carSpriteList.isEmpty()) {
            return;
        }

        Image image;

        double roadSpeed = road.getSpeed() / 2.0;

        double speed;


        if (state.getSpriteDirection() == Constants.ONCOMING_CAR) {

            image = oncomingCarImage.get(state.getSpriteType());

            speed = roadSpeed + road.getSpeed();

        } else {

            image = passingCarImage.get(state.getSpriteType());

            speed = roadSpeed;

        }

        double y = -image.getHeight();

        CarSprite sprite = new CarSprite(image, playfieldLayer, state.getSpriteX(),
                y, speed, 0, speed, state.getSpriteDirection(), state.getSpriteType());

        carSpriteList.add(sprite);


    }

    private void initializeInput() {

        scene.setOnKeyPressed(keyEvent -> {

            boolean isPressed = keyEvent.getCode() == KeyCode.B;

            if (isPressed && !botMode) {

                player.setControllable(new BotController());

                botMode = true;

                System.out.println("Bot Mode");


            } else if (isPressed && botMode) {

                player.setControllable(new PlayerController(player));

                botMode = false;

                System.out.println("God Mode");
            }

        });

    }

    private void spawnClouds() {

        if (random.nextInt(Constants.CLOUD_RANDOMLESS) != 0) {

            return;
        }

        Pane layer;
        Image image;

        double speed;
        double opacity;

        double x;
        double y;

        // place random cloud on upper or lower layer with different opacity
        if (random.nextInt(2) == 0) {

            layer = lowerCloudLayer;
            opacity = 0.8;                      //hello from uvazhayemy tovarishch
            speed = random.nextDouble() + 1.0;

        } else {

            layer = upperCloudLayer;
            opacity = 0.5;
            speed = random.nextDouble() + 2.0;

        }

        if (difficultyLevel == Constants.HARD_LEVEL) {

            opacity = 1.0;

        }

        image = cloudImage.get(random.nextInt(cloudImage.size()));

        x = random.nextDouble() * Constants.WINDOW_WIDTH - image.getWidth() / 2.0;
        y = -image.getHeight();

        CloudSprite cloudSprite = new CloudSprite(image, layer, x, y, opacity, speed);

        cloudSpriteList.add(cloudSprite);

    }


    private void checkRemovability(List<AbstractSprite> spriteList) {

        spriteList.forEach(AbstractSprite::checkRemovability);

    }

    private void moveSprites(List<AbstractSprite> spriteList) {

        spriteList.forEach(AbstractSprite::move);

    }

    private void renderSprites(List<AbstractSprite> spriteList) {

        spriteList.forEach(AbstractSprite::update);

    }

    private void removeSprites(List<AbstractSprite> spriteList) {

        Iterator iterator = spriteList.iterator();

        // Da, java moget v funcionalschinu
        while (iterator.hasNext()) {

            AbstractSprite sprite = (AbstractSprite) iterator.next();

            if (sprite.isRemovable()) {

                sprite.removeFromLayer();
                iterator.remove();

            }
        }
    }


    private void spawnCar() {

        if (!carSpriteList.isEmpty()) {

            return;

        }

        int carType = random.nextInt(2);

        createCar(carType);
    }

    public int nextIntInRangeButExclude(int start, int end, int... excludes) {

        int rangeLength = end - start - excludes.length;
        int randomInt = random.nextInt(rangeLength) + start;

        for (int i = 0; i < excludes.length; i++) {
            if (excludes[i] > randomInt) {
                return randomInt;
            }

            randomInt++;
        }

        return randomInt;
    }

    public int getRandomWithExclusion(Random rnd, int start, int end, int... exclude) {
        int random = start + rnd.nextInt(end - start + 1 - exclude.length);
        for (int ex : exclude) {
            if (random < ex) {
                break;
            }
            random++;
        }
        return random;
    }


    private void createCar(int carDirection) {

        Image image;

        double roadSpeed = road.getSpeed() / 2.0;

        double speed;

        double x;
        double y;

        int randomOffset = random.nextInt(Constants.LANE_OFFSET);

        int spriteType = getRandomWithExclusion(random, 0, 2, spawnedCar);

        spawnedCar = spriteType;

        System.out.println(spawnedCar);

        if (carDirection == Constants.ONCOMING_CAR) {


            image = oncomingCarImage.get(spriteType);

            x = (Constants.ONCOMING_LANE + Constants.LEFT_LANE - image.getWidth()) / 2.0 + randomOffset;
            y = -image.getHeight();

            speed = roadSpeed + road.getSpeed();


        } else {

            image = passingCarImage.get(spriteType);

            x = (Constants.RIGTH_LANE + Constants.PASSING_LANE - image.getWidth()) / 2.0 - randomOffset;
            y = -image.getHeight();

            speed = roadSpeed;

        }


        CarSprite sprite = new CarSprite(image, playfieldLayer, x, y, speed, 0, speed, carDirection, spriteType);

        carSpriteList.add(sprite);

    }

    private void createPlayer() {

        double x = (Constants.RIGTH_LANE + Constants.LEFT_LANE) / 2.0 - Constants.CAR_LEFT_X;
        double y = Constants.WINDOW_HEIGHT * 0.7;

        player = new Player(setImage("resources/sprites/player.png"), playfieldLayer, x, y,
                0.0, 0.0, Constants.PLAYER_SPEED, scene);

        player.setControllable(new PlayerController(player));

        playerList.add(player);

    }


    private void loadGame() {

        for (int i = 1; i <= 3; i++) {

            cloudImage.put(i - 1, setImage("resources/sprites/cloud" + i + ".png"));


        }


        for (int i = 1; i <= 3; i++) {

            passingCarImage.put(i - 1, setImage("resources/sprites/passingCar" + i + ".png"));

        }

        for (int i = 1; i <= 3; i++) {

            oncomingCarImage.put(i - 1, setImage("resources/sprites/oncomingCar" + i + ".png"));

        }

        road = new Road();

        backgroudLayer.getChildren().addAll(road.getFirstRoadSegment(), road.getSecondRoadSegment());

    }


    private void createDebugOverlay() {

        informationLabel = new Label();
        informationLabel.setTextFill(Color.WHITE);

        informationLabel.setFont(Font.loadFont("file:resources/fonts/AmericanCapitan.tff", 24));

        debugLayer.getChildren().add(informationLabel);

    }

    private void updateFps() {

        frameCount++;


        long currentTime = System.currentTimeMillis();

        if (currentTime - previousTime >= 1000) {

            fpsCurrent = frameCount;
            previousTime = currentTime;
            frameCount = 0;

        }

        vRoad = road.getSpeed() * fpsCurrent;
        vOncoming = 3.0 / 2.0 * vRoad;

    }

    private void updateScore() {

        synchronized (updaterMonitor) {

            updaterMonitor.notify();

        }

    }

    public Scene getScene() {
        return scene;
    }

    public class Finalizer extends Thread {

        private boolean isReplay;
        private Object finalizerMonitor;

        public Finalizer(boolean isReplay, Object finalizerMonitor) {

            this.isReplay = isReplay;
            this.finalizerMonitor = finalizerMonitor;

        }

        public void run() {

            synchronized (finalizerMonitor) {

                try {
                    finalizerMonitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (!isReplay) {

                Writer writer = new Writer(gameStates);

                writer.start();

                try {
                    writer.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }


            Platform.runLater(() -> {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("EOG");
                alert.setHeaderText(null);
                alert.setContentText("Takie dela(!");

                Optional<ButtonType> result = alert.showAndWait();

                if (result.get() == ButtonType.OK) {

                    Platform.exit();

                }

            });


        }
    }


}
