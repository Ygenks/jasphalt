package jasphalt;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class CarSprite extends AbstractSprite {

    private double speed;
    private int carDirection;



    // 0 - orange car
    // 1 - truck
    // 2 - taxi
    private int spriteType;



    public CarSprite(Image image, Pane layer, double x, double y, double dy, double dx, double speed, int carDirection, int spriteType) {

        super(image, layer, x, y, dy, dx);

        this.speed = speed;
        this.carDirection = carDirection;

        this.spriteType = spriteType;
    }


    public int getCarDirection() {
        return carDirection;
    }

    public void setCarDirection(int carDirection) {
        this.carDirection = carDirection;
    }

    public int getSpriteType() {
        return spriteType;
    }

    public void setSpriteType(int spriteType) {
        this.spriteType = spriteType;
    }
}
