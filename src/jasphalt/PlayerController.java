package jasphalt;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.util.BitSet;


/**
 * Player controller class is responsible for handling keyboard input.
 * It could process multiple keys pressed via BitSet.
 */
public class PlayerController implements Controllable {

    private BitSet keyboardBitSet = new BitSet();

    private KeyCode upKey = KeyCode.W;
    private KeyCode downKey = KeyCode.S;
    private KeyCode leftKey = KeyCode.A;
    private KeyCode rightKey = KeyCode.D;


    public PlayerController(Player player) {

        addListeners(player);

    }

    /**
     * Sets keyboard events handlers.
     * @param player
     */
    public void addListeners(Player player) {

        player.getScene().addEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
        player.getScene().addEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);

    }

    /**
     * Removes keyboard event handlers.
     * @param player
     */
    public void removeListeners(Player player) {

        player.getScene().removeEventFilter(KeyEvent.KEY_PRESSED, keyPressedEventHandler);
        player.getScene().removeEventFilter(KeyEvent.KEY_RELEASED, keyReleasedEventHandler);

    }

    /**
     * Maps pressed keys with true value in BitSet.
     */
    private EventHandler<KeyEvent> keyPressedEventHandler = event -> {

        keyboardBitSet.set(event.getCode().ordinal(), true);

    };

    /**
     * Maps released keys with false value in BitSet.
     */
    private EventHandler<KeyEvent> keyReleasedEventHandler = event -> {

        keyboardBitSet.set(event.getCode().ordinal(), false);

    };


    public boolean isMoveUp() {
        return keyboardBitSet.get(upKey.ordinal()) && !keyboardBitSet.get(downKey.ordinal());
    }

    public boolean isMoveDown() {
        return keyboardBitSet.get(downKey.ordinal()) && !keyboardBitSet.get(upKey.ordinal());
    }

    public boolean isMoveLeft() {
        return keyboardBitSet.get(leftKey.ordinal()) && !keyboardBitSet.get(rightKey.ordinal());
    }

    public boolean isMoveRight() {
        return keyboardBitSet.get(rightKey.ordinal()) && !keyboardBitSet.get(leftKey.ordinal());
    }

    /**
     * {@inheritDoc}
     * @param player I tak ponyanto
     */
    @Override
    public void move(Player player) {

        if (isMoveUp()) {

            System.out.println("nedaun");
            player.setDy(-player.getSpeed());

        } else if (isMoveDown()) {

            System.out.println("daun");
            player.setDy(player.getSpeed());

        } else {

            player.setDy(0.0);

        }

        if (isMoveLeft()) {

            System.out.println("lefch");
            player.setDx(-player.getSpeed());

        } else if (isMoveRight()) {

            System.out.println("raich");
            player.setDx(player.getSpeed());

        } else {

            player.setDx(0.0);

        }


    }
}
