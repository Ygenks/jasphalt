package jasphalt;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public abstract class AbstractSprite {

    protected Image image;
    protected ImageView imageView;
    protected Pane layer;


    protected double x;
    protected double y;

    protected double dx;
    protected double dy;

    protected double width;
    protected double height;


    protected boolean removable;

    public double getDx() {
        return dx;
    }

    public void setDx(double dx) {
        this.dx = dx;
    }

    public double getDy() {
        return dy;
    }

    public void setDy(double dy) {
        this.dy = dy;
    }

    public AbstractSprite(Image image, Pane layer, double x, double y, double dy, double dx) {

        this.image = image;
        this.layer = layer;

        this.x = x;
        this.y = y;

        this.dy = dy;
        this.dx = dx;

        this.imageView = new ImageView(image);
        this.imageView.relocate(x, y);

        this.width = image.getWidth();
        this.height = image.getHeight();

        addToLayer();

    }


    public void addToLayer() {

        layer.getChildren().add(this.imageView);

    }

    public void removeFromLayer() {

        layer.getChildren().remove(this.imageView);

    }

    public void move() {

        x += dx;
        y += dy;
    }

    public void update() {

        imageView.relocate(x, y);

    }

    /**
     * Checking sprite position on the screen and setting removable state if sprite is outside the screen.
     */
    public void checkRemovability() {

        boolean crossedTopOrBottom = crossedTopBorder() || crossedBottomBorder();
        boolean crossedLeftOrRight = crossedLeftBorder() || crossedRightBorder();

        if (crossedTopOrBottom || crossedLeftOrRight) {

            removable = true;

        }

    }

    /**
     * Checking sprite for getting below the bottom of the screen.
     *
     * @return Sprite status.
     */
    public boolean crossedBottomBorder() {

        if (y > Constants.WINDOW_HEIGHT) {

            return true;

        }
        return false;
    }

    /**
     * Checking sprite for getting above the top of the screen.
     *
     * @return Sprite status.
     */
    public boolean crossedTopBorder() {

        if (y + height < 0) {

            return true;

        }
        return false;
    }

    /**
     * Checking sprite for getting across the left border of the screen.
     *
     * @return Sprite status.
     */
    public boolean crossedLeftBorder() {

        if (x + width < 0) {

            return true;

        }
        return false;
    }

    /**
     * Checking sprite for getting across the right border of the screen.
     *
     * @return Sprite status.
     */
    public boolean crossedRightBorder() {

        if (x > Constants.WINDOW_WIDTH) {

            return true;

        }
        return false;
    }


    public boolean isRemovable() {
        return removable;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
