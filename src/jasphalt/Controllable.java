package jasphalt;

/**
 * Interface of strategy which determines the controller of player car.
 */
public interface Controllable {

    /**
     * Moves player on the scene and logs performed actions.
     * @param player
     */
    void move(Player player);

}
