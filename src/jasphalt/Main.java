package jasphalt;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.FillTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.*;

import static jasphalt.Utils.readStatistics;
import static jasphalt.Utils.setImage;


public class Main extends Application {


    @Override
    public void start(Stage primaryStage) {

        Pane root = new Pane();

        Scene scene = new Scene(root, Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT);

        ImageView imageView = new ImageView(setImage("resources/sprites/road.png"));

        imageView.setFitHeight(Constants.WINDOW_HEIGHT);
        imageView.setFitWidth(Constants.WINDOW_WIDTH);

        root.getChildren().add(imageView);

        MenuItem newGame = new MenuItem("New Game");
        MenuItem replays = new MenuItem("Replays");
        MenuItem exitGame = new MenuItem("Exit");
        MenuItem statistics = new MenuItem("Statistics");
        SubMenu mainMenu = new SubMenu(newGame, replays, statistics, exitGame);


        MenuItem normalLevel = new MenuItem("Normal level");
        MenuItem hardLevel = new MenuItem("Hard level");
        MenuItem newGameBack = new MenuItem("Back");
        SubMenu newGameMenu = new SubMenu(normalLevel, hardLevel, newGameBack);

        MenuItem replaySort = new MenuItem("Sort replays");
        MenuItem replayStatistics = new MenuItem("Show statistics");
        MenuItem replaySequence = new MenuItem("Show sequence");
        MenuItem statisticsBack = new MenuItem("Back");
        SubMenu statisticsMenu = new SubMenu(replaySort, replayStatistics, replaySequence, statisticsBack);

        MenuItem gameStatisticsBack = new MenuItem("Back");
        MenuItem updateStatistics = new MenuItem("Update");
        final String[] gameStatistics = new String[1];
        final Label[] statisticsLabel = {new Label()};

        MenuItem gameSequenceBack = new MenuItem("Back");
        MenuItem updateSequence = new MenuItem("Update");
        final String[] gameSequence = new String[1];
        final Label[] sequenceLabel = {new Label()};


        MenuBox menuBox = new MenuBox(mainMenu);

        normalLevel.setOnMouseClicked(event -> {

            Game game = new Game(Constants.NORMAL_LEVEL);
            primaryStage.setScene(game.getScene());
            game.startGame();

        });

        hardLevel.setOnMouseClicked(mouseEvent -> {

            Game game = new Game(Constants.HARD_LEVEL);
            primaryStage.setScene(game.getScene());
            game.startGame();

        });


        newGame.setOnMouseClicked(mouseEvent -> {

            menuBox.setSubMenu(newGameMenu);

        });

        newGameBack.setOnMouseClicked(mouseEvent -> {

            menuBox.setSubMenu(mainMenu);

        });

        replays.setOnMouseClicked(mouseEvent -> {


            final FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose replay");


            fileChooser.setInitialDirectory(new File(System.getProperty("user.home")
                    + "/programming/Java/Jasphalt/replays/"));


            File replayFile = fileChooser.showOpenDialog(null);

            String replayName = replayFile.getName();


            Game game = new Game(Constants.NORMAL_LEVEL);
            primaryStage.setScene(game.getScene());
            game.replay(replayName);

        });

        exitGame.setOnMouseClicked(event -> Platform.exit());


        replaySort.setOnMouseClicked(mouseEvent -> {

            long scalaTime = ScalaThings.sortReplays();

            long javaTime = Utils.javaSort();


        });

        gameStatisticsBack.setOnMouseClicked(mouseEvent -> {

            root.getChildren().removeAll(statisticsLabel[0], gameStatisticsBack, updateStatistics);
            menuBox.setSubMenu(statisticsMenu);
            root.getChildren().add(menuBox);

        });

        updateStatistics.setOnMouseClicked(mouseEvent -> {

            ScalaThings.updateStatistics();
            gameStatistics[0] = readStatistics("resources/statistics.txt");
            statisticsLabel[0].setText(gameStatistics[0]);

        });

        replayStatistics.setOnMouseClicked(mouseEvent -> {

            root.getChildren().remove(menuBox);

            gameStatistics[0] = readStatistics("resources/statistics.txt");

            statisticsLabel[0] = new Label(gameStatistics[0]);

            statisticsLabel[0].setTextFill(Color.WHITESMOKE);
            statisticsLabel[0].setFont(Font.loadFont("file:resources/fonts/AmericanCapitan.tff", 36));

            statisticsLabel[0].setLayoutX(15);
            statisticsLabel[0].setLayoutY(15);

            Rectangle rectangle = new Rectangle(Constants.WINDOW_WIDTH,
                    Constants.WINDOW_HEIGHT, Color.rgb(64, 75, 64));

            rectangle.setOpacity(0.5);

            gameStatisticsBack.setLayoutY(Constants.WINDOW_HEIGHT / 1.25);

            gameStatisticsBack.setTranslateX((Constants.WINDOW_WIDTH - 175) / 2.0);

            updateStatistics.setLayoutY(Constants.WINDOW_HEIGHT / 1.5);

            updateStatistics.setTranslateX((Constants.WINDOW_WIDTH - 175) / 2.0);


            root.getChildren().addAll(rectangle, statisticsLabel[0], gameStatisticsBack, updateStatistics);

        });


        gameSequenceBack.setOnMouseClicked(mouseEvent -> {

            root.getChildren().removeAll(sequenceLabel[0], gameSequenceBack, updateSequence);
            menuBox.setSubMenu(statisticsMenu);
            root.getChildren().add(menuBox);

        });

        updateSequence.setOnMouseClicked(mouseEvent -> {

            ScalaThings.sequencer();
            gameSequence[0] = readStatistics("resources/sequence.txt");
            sequenceLabel[0].setText(gameSequence[0]);

        });

        replaySequence.setOnMouseClicked(mouseEvent -> {

            root.getChildren().remove(menuBox);

            gameSequence[0] = readStatistics("resources/sequence.txt");

            sequenceLabel[0] = new Label(gameSequence[0]);

            sequenceLabel[0].setTextFill(Color.WHITESMOKE);
            sequenceLabel[0].setFont(Font.loadFont("file:resources/fonts/AmericanCapitan.tff", 36));

            sequenceLabel[0].setLayoutX(15);
            sequenceLabel[0].setLayoutY(15);

            Rectangle rectangle = new Rectangle(Constants.WINDOW_WIDTH,
                    Constants.WINDOW_HEIGHT, Color.rgb(64, 75, 64));

            rectangle.setOpacity(0.5);

            gameSequenceBack.setLayoutY(Constants.WINDOW_HEIGHT / 1.25);

            gameSequenceBack.setTranslateX((Constants.WINDOW_WIDTH - 175) / 2.0);

            updateSequence.setLayoutY(Constants.WINDOW_HEIGHT / 1.5);

            updateSequence.setTranslateX((Constants.WINDOW_WIDTH - 175) / 2.0);


            root.getChildren().addAll(rectangle, sequenceLabel[0], gameSequenceBack, updateSequence);

        });


        statisticsBack.setOnMouseClicked(mouseEvent1 -> {

            menuBox.setSubMenu(mainMenu);

        });

        statistics.setOnMouseClicked(mouseEvent -> {

            menuBox.setSubMenu(statisticsMenu);

        });

        root.getChildren().addAll(menuBox);


        scene.setOnKeyPressed(event -> {

            if (event.getCode() == KeyCode.ESCAPE) {

                FadeTransition transition = new FadeTransition(Duration.seconds(0.5), menuBox);

                if (!menuBox.isVisible()) {

                    transition.setFromValue(0);
                    transition.setToValue(1);
                    transition.play();

                    menuBox.setVisible(true);

                } else {

                    transition.setFromValue(1);
                    transition.setToValue(0);
                    transition.setOnFinished(actionEvent -> menuBox.setVisible(false));
                    transition.play();

                }
            }
        });

        primaryStage.setTitle("Jasphalt");
        primaryStage.setResizable(false);

        primaryStage.setScene(scene);

        primaryStage.show();
    }

    private static class MenuItem extends StackPane {

        public MenuItem(String name) {

            Rectangle rectangle = new Rectangle(200, 45, Color.DARKGRAY);
            rectangle.setOpacity(0.5);

            Text text = new Text(name);
            text.setFill(Color.WHITE);

            text.setFont(Font.loadFont("file:resources/fonts/AmericanCapitan.tff", 36));

            setAlignment(Pos.CENTER);
            getChildren().addAll(rectangle, text);

            FillTransition transition = new FillTransition(Duration.seconds(0.5), rectangle);

            setOnMouseEntered(event -> {

                transition.setFromValue(Color.DARKGRAY);
                transition.setToValue(Color.rgb(40, 203, 45));
                transition.setCycleCount(Animation.INDEFINITE);
                transition.setAutoReverse(true);
                transition.play();

            });

            setOnMouseExited(event -> {

                transition.stop();
                rectangle.setFill(Color.DARKGRAY);

            });
        }
    }

    private static class MenuBox extends Pane {

        public static SubMenu subMenu;

        public MenuBox(SubMenu subMenu) {

            MenuBox.subMenu = subMenu;

            setVisible(false);

            Rectangle rectangle = new Rectangle(Constants.WINDOW_WIDTH,
                    Constants.WINDOW_HEIGHT, Color.rgb(64, 75, 64));

            rectangle.setOpacity(0.5);

            getChildren().addAll(rectangle, subMenu);
        }

        public void setSubMenu(SubMenu subMenu) {

            getChildren().remove(MenuBox.subMenu);

            MenuBox.subMenu = subMenu;

            getChildren().add(MenuBox.subMenu);

        }
    }


    private static class SubMenu extends VBox {

        public SubMenu(MenuItem... items) {

            setSpacing(15);
            setTranslateY(Constants.WINDOW_HEIGHT / 3.0);

            setTranslateX((Constants.WINDOW_WIDTH - 175) / 2.0);

            for (MenuItem item : items) {

                getChildren().addAll(item);

            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}






