package jasphalt;

import javafx.animation.Animation;
import javafx.animation.FillTransition;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class MenuItem extends StackPane {

    private Text text;

    public MenuItem(String name) {

        text = new Text(name);
        text.setFill(Color.WHITE);
        text.setFont(Font.loadFont("file:resources/fonts/AmericanCapitan.ttf", 18));

        Rectangle rectangle = new Rectangle(100, 40);
        rectangle.setOpacity(0.8);

        setAlignment(Pos.CENTER);
        getChildren().addAll(rectangle, text);

        FillTransition fillTransition = new FillTransition(Duration.seconds(0.2), rectangle);

        setOnMouseEntered(mouseEvent -> {

            fillTransition.setFromValue(Color.YELLOW);
            fillTransition.setToValue(Color.GRAY);
            fillTransition.setCycleCount(Animation.INDEFINITE);
            fillTransition.play();
        });

        setOnMouseExited(mouseEvent -> {

           fillTransition.stop();
            rectangle.setFill(Color.YELLOW);
        });
    }
}
