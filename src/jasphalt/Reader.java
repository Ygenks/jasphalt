package jasphalt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.LinkedList;

public class Reader extends Thread {

    private String name;

    public Reader(String name) {

        this.name = name;
    }

    private LinkedList<GameState> states = new LinkedList<>();


    public void run() {

        try(FileInputStream fileInputStream = new FileInputStream("replays/" + name);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {

            states = (LinkedList<GameState>) objectInputStream.readObject();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        System.out.print("Read successful");

    }

    public LinkedList<GameState> getStates() {

        return states;

    }

}
