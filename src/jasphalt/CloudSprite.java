package jasphalt;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class CloudSprite extends AbstractSprite {

    private double opacity;
    private double speed;

    public CloudSprite(Image image, Pane layer, double x, double y, double opacity, double speed) {

        super(image, layer, x, y, speed, 0.0);

        this.opacity = opacity;
        this.speed = speed;

        imageView.setOpacity(opacity);

    }

}
