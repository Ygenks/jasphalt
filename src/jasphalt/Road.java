package jasphalt;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import static jasphalt.Utils.setImage;

/**
 * The Road class represents game road and is used for creating game background.
 */
public class Road {

    private ImageView firstRoadSegment;
    private ImageView secondRoadSegment;

    private Image roadImage;

    private final double speed = 3.0;

    /**
     * Loads road image and sets default
     * position on the pane.
     */
    public Road() {

        roadImage = setImage("resources/sprites/road.png");

        firstRoadSegment = new ImageView();
        secondRoadSegment = new ImageView();


        firstRoadSegment.setImage(roadImage);
        secondRoadSegment.setImage(roadImage);

        firstRoadSegment.relocate(0, 0);
        secondRoadSegment.relocate(0, -roadImage.getHeight()); // place image above the screen

    }

    /**
     * Updates road position on the screen.
     */
    public void updateRoad() {

        double firstRoadY = firstRoadSegment.getLayoutY() + speed;
        double secondRoadY = secondRoadSegment.getLayoutY() + speed;

        // if road segment is out of bounds
        if (secondRoadY >= 0.0 && firstRoadY >= Constants.WINDOW_HEIGHT) {

            firstRoadY = -Constants.WINDOW_HEIGHT + secondRoadY; // move it up and add delta between pictures

        }

        if (firstRoadY >= 0.0 && secondRoadY >= Constants.WINDOW_HEIGHT) {

            secondRoadY = -Constants.WINDOW_HEIGHT + firstRoadY;

        }

        firstRoadSegment.setLayoutY(firstRoadY);
        secondRoadSegment.setLayoutY(secondRoadY);
    }


    public ImageView getFirstRoadSegment() {

        return firstRoadSegment;

    }

    public ImageView getSecondRoadSegment() {

        return secondRoadSegment;

    }

    public double getSpeed() {

        return speed;

    }
}
