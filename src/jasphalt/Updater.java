package jasphalt;

import javafx.application.Platform;
import javafx.scene.control.Label;

public class Updater extends Thread {

    private Label informationLabel;
    private Object updaterMonitor;

    private long score;
    private Road road;
    private int difficulty;

    private boolean isRunning = true;

    public Updater(Label informationLabel, Object monitor, int difficulty, long score, Road road) {

        this.informationLabel = informationLabel;

        this.updaterMonitor = monitor;

        this.difficulty = difficulty;

        this.score = score;

        this.road = road;
    }

    public void run() {

        int scoreCoefficient;

        if (difficulty == Constants.HARD_LEVEL) {

            scoreCoefficient = 2;

        } else {

            scoreCoefficient = 1;
        }

        while (true) {


            if (!isRunning) {

                return;

            }

            score += 5 * scoreCoefficient;

            synchronized (updaterMonitor) {

                try {
                    updaterMonitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            Platform.runLater(() -> informationLabel.setText("Score: " + score));
            Platform.runLater(() -> road.updateRoad());

        }
    }

    public void kill() {

        isRunning = false;

    }
}
