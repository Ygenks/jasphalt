package jasphalt;

import java.io.Serializable;

public class GameState implements Serializable {

    private static final long serialVersionUID = 2281488L;

    private double spriteX;

    private long score;

    private int difficulty;

    private int spriteDirection;
    private int spriteType;

    private double playerX;
    private double playerY;



    public GameState(Player player, CarSprite sprite, long score, int difficulty) {

        this.score = score;
        this.difficulty = difficulty;

        playerX = player.getX();
        playerY = player.getY();

        spriteX = sprite.getX();

        spriteDirection = sprite.getCarDirection();
        spriteType = sprite.getSpriteType();


    }

    public double getPlayerX() {
        return playerX;
    }

    public double getPlayerY() {
        return playerY;
    }

    public int getSpriteDirection() {
        return spriteDirection;
    }

    public int getSpriteType() {
        return spriteType;
    }

    public double getSpriteX() {
        return spriteX;
    }

    public long getScore() {
        return score;
    }

    public int getDifficulty() {
        return difficulty;
    }
}
