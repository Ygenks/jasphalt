package jasphalt;

public class Constants {

    public static final int WINDOW_HEIGHT = 512;
    public static final int WINDOW_WIDTH = 512;
    public static final int RIGTH_LANE = 369;
    public static final int LEFT_LANE = 149;
    /*TODO rename lane shit names*/
    public static final int ONCOMING_LANE = 214;
    public static final int PASSING_LANE = 295;

    public static final int CAR_LEFT_X = 40;
    public static final int CAR_RIGHT_X = 86;
//     12 40 86 119
    public static final int CAR_UPPER_Y = 18;
    public static final int CAR_LOWER_Y = 119;

    public static final double PLAYER_SPEED = 4.0;


    public static final int CLOUD_RANDOMLESS = 120;
    public static final int LANE_OFFSET = 45;


    public static final int ONCOMING_CAR = 0;
    public static final int PASSING_CAR = 1;

    public static final int NORMAL_LEVEL = 0;
    public static final int HARD_LEVEL = 1;
}
