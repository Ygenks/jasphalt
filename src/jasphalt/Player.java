package jasphalt;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;

/**
 * Represents player car.
 */
public class Player extends AbstractSprite {

    private double speed;

    private double playerMinX;
    private double playerMaxX;
    private double playerMinY;
    private double playerMaxY;

    private boolean moveBotLeft;
    private boolean moveBotRight;

    private Scene scene;

    private Controllable controllable;



    public Player(Image image, Pane layer, double x, double y, double dy, double dx,
                  double speed, Scene scene) {

        super(image, layer, x, y, dy, dx);

        this.scene = scene;
        this.speed = speed;

        setBounds();

    }

    /**
     * Sets road bounds for player car.
     */
    private void setBounds() {

         playerMinX = Constants.LEFT_LANE - width / 2.0;
         playerMaxX = Constants.RIGTH_LANE - width / 2.0;

         playerMinY = 0 - height / 2.0;
         playerMaxY = Constants.WINDOW_HEIGHT - height / 2.0;

    }

    /**
     * Checks player coordinates for lying on the road.
     */
    public void checkBounds() {


        if(Double.compare(y, playerMinY) < 0) {

            y = playerMaxY;

        } else if (Double.compare(y, playerMaxY) > 0) {

            y = playerMaxY;
        }

        if(Double.compare(x, playerMinX) < 0) {

            x = playerMinX;

        } else if(Double.compare(x, playerMaxX) > 0) {

            x = playerMaxX;
        }

    }

    /**
     * Checks player collisions.
     * @param sprite Car sprite.
     * @return Returns true if cars collided.
     */
    public boolean checkCollisions(AbstractSprite sprite) {

        Rectangle playerCarRectangle = new Rectangle(imageView.getLayoutX() + Constants.CAR_LEFT_X,
                imageView.getLayoutY() + Constants.CAR_UPPER_Y,
                Constants.CAR_RIGHT_X - Constants.CAR_LEFT_X,
                Constants.CAR_LOWER_Y - Constants.CAR_UPPER_Y);

//        playerCarRectangle.setFill(Color.RED);
//        Pane kek = new Pane(playerCarRectangle);
//        playfieldLayer.getChildren().add(kek);

        Rectangle spriteCarRectangle = new Rectangle(sprite.getImageView().getLayoutX() +
                Constants.CAR_LEFT_X,
                sprite.getImageView().getLayoutY() + Constants.CAR_UPPER_Y,
                Constants.CAR_RIGHT_X - Constants.CAR_LEFT_X,
                Constants.CAR_LOWER_Y - Constants.CAR_UPPER_Y);

//        spriteCarRectangle.setFill(Color.BLUE);
//        kek.getChildren().add(spriteCarRectangle);


        if (playerCarRectangle.intersects(spriteCarRectangle.getLayoutBounds())) {
            return true;
        }

        return false;
    }

    public void checkBotCollisions(AbstractSprite carSprite) {


        //Constant approved by some physics
        double dodgeDistance = 3 / 2 * (Constants.CAR_RIGHT_X - Constants.CAR_LEFT_X);


        Rectangle playerCarRectangle = new Rectangle(imageView.getLayoutX() + Constants.CAR_LEFT_X,
                imageView.getLayoutY() + Constants.CAR_UPPER_Y - dodgeDistance,
                Constants.CAR_RIGHT_X - Constants.CAR_LEFT_X,
                Constants.CAR_LOWER_Y - Constants.CAR_UPPER_Y + dodgeDistance);


//        playerCarRectangle.setFill(Color.RED);
//        playfieldLayer.getChildren().add(playerCarRectangle);

        Rectangle spriteCarRectangle = new Rectangle(carSprite.getImageView().getLayoutX() +
                Constants.CAR_LEFT_X,
                carSprite.getImageView().getLayoutY() + Constants.CAR_UPPER_Y,
                Constants.CAR_RIGHT_X - Constants.CAR_LEFT_X,
                Constants.CAR_LOWER_Y - Constants.CAR_UPPER_Y);

//        spriteCarRectangle.setFill(Color.BLUE);
//        playfieldLayer.getChildren().add(spriteCarRectangle);


        if (playerCarRectangle.intersects(spriteCarRectangle.getLayoutBounds())) {

            double carWidth = Constants.CAR_RIGHT_X - Constants.CAR_LEFT_X;
            double playerCarCenterX = playerCarRectangle.getX() + carWidth / 2.0;
            double botCarCenterX = spriteCarRectangle.getX() + carWidth / 2.0;

            if (playerCarCenterX < botCarCenterX) {
                moveBotLeft = true;
                moveBotRight = false;
            } else {
                moveBotLeft = false;
                moveBotRight = true;
            }
        } else {

            moveBotLeft = false;
            moveBotRight = false;

        }

    }


    @Override
    public void move() {

        controllable.move(this);
        super.move();

    }

    public double getSpeed() {
        return speed;
    }

    public Controllable getControllable() {
        return controllable;
    }

    public void setControllable(Controllable controllable) {
        this.controllable = controllable;
    }

    public boolean isMoveBotLeft() {
        return moveBotLeft;
    }

    public boolean isMoveBotRight() {
        return moveBotRight;
    }

    public Scene getScene() {
        return scene;
    }
}


