package jasphalt;

public class BotController implements Controllable{

    /**
     * {@inheritDoc}
     * @param player
     */
    @Override
    public void move(Player player) {

        if (player.isMoveBotRight()) {

            System.out.println("bot-raich");
            player.setDx(player.getSpeed());

        } else if (player.isMoveBotLeft()) {

            System.out.println("bot-lefch");
            player.setDx(-player.getSpeed());

        } else {

            player.setDx(0.0);

        }
    }
}
