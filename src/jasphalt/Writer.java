package jasphalt;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

public class Writer extends Thread {

    private LinkedList<GameState> states;

    public Writer(LinkedList<GameState> states) {

        this.states = states;

    }


    public void run() {

        DateFormat dateFormat = new SimpleDateFormat("dd_HH_mm_ss");
        Date date = new Date();

        String name = new String(dateFormat.format(date));

        // nado ili net?
        if(states.isEmpty()) {

            System.out.println("Empty list");
            return;

        }

        try (FileOutputStream fileOutputStream = new FileOutputStream("replays/" + name);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {


            objectOutputStream.writeObject(states);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Successful write");

    }

}
